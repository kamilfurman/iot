int Trig = 2;   // Numer pinu wyzwolenia
int Echo = 3;   // Numer pinu odpowiedzi
int Red = 4;    // Numer pinu - dioda czerwona
int Green = 5;  // Numer Pinu - dioda zielona
int BEEP = 6;

long EchoTime;  // Czas trwania sygnału ECHO
int  Distance;  // Odległość w centymetrach
int  MaximumRange = 20000; // Maksymalna odległość
int  MinimumRange = 2;   // Minimalna odległość

void setup()
{
  // Inicjalizacja portu szeregowego
  Serial.begin(9600);

  // Konfiguracja pinów
  pinMode(Trig, OUTPUT);
  pinMode(Echo, INPUT);
  pinMode(Red, OUTPUT);
  pinMode(Green, OUTPUT);
  pinMode(BEEP, OUTPUT); 
}

void loop()
{
  // Ustawiamy TRIG w stan niski na 2us
  digitalWrite(Trig, LOW);
  delayMicroseconds(2);

  // Ustawiamy TRIG w stan wysoki na 10us
  digitalWrite(Trig, HIGH);
  delayMicroseconds(10);

  // Ustawiamy TRIG w stan niski - rozpoczynamy pomiar
  digitalWrite(Trig, LOW);

  // Odczytujamy czas trwania stanu wysokiego na pinie ECHO
  EchoTime = pulseIn(Echo, HIGH);

  // Obliczamy odległość
  Distance = EchoTime / 58;

  // Sprawdzamy zakres pomiarowy
  if (Distance >= MaximumRange || Distance <= MinimumRange)
  {
    Serial.println("Poza zakresem");  
  } 
  else  
  {

    if (Distance < 110)
    {
      digitalWrite(Red, HIGH);      
      digitalWrite(Green, LOW);
      digitalWrite(BEEP, HIGH);
      digitalWrite(BEEP, LOW);
      digitalWrite(BEEP, HIGH);
    } 
    else
    {
      digitalWrite(Red, LOW);      
      digitalWrite(Green, HIGH);
    }    

    Serial.print("Odleglosc: ");
    Serial.println(Distance);
  }

  // Opóźnienie kolejnego pomiaru
  delay(10);
}

